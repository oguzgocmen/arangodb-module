from unittest import TestCase
from src import Main
import arango, json

class TestMain(TestCase):
    def test_open_database(self):
        visualizer = Main.Visualizer()
        self.assertTrue(visualizer.open_database("test-db", 8529))
        #self.assertTrue(visualizer.open_database(85))


    def test_create_graph(self):
        visualizer = Main.Visualizer()
        db = visualizer.open_database(8529)
        self.assertTrue(visualizer.create_graph(db,"asdasd"))
        #self.fail()

    def test_create_vertices(self):
        visualizer = Main.Visualizer()
        db = visualizer.open_database(8529)
        graph = visualizer.init_graph(db, "test-db")
        vertex = visualizer.create_vertices(graph, "test_vertex")
        self.assertIsInstance(vertex, arango.collection.VertexCollection)

    def test_create_edges(self):
        visualizer = Main.Visualizer()
        db = visualizer.open_database(8529)
        graph = visualizer.init_graph(db, "test-db")
        edge = visualizer.create_edges(graph, "test_edge",'from','to')
        self.assertIsInstance(edge, arango.collection.EdgeCollection)

    def test_init_graph(self):
        visualizer = Main.Visualizer()
        db = visualizer.open_database(8529)
        self.assertTrue(visualizer.init_graph(db,'123'))

    def test_determine_fields(self):
        visualizer = Main.Visualizer()
        test_list ='{"created_at":"Fri Aug 10 07:44:45 +0000 2018","id":1027823049943351297,"id_str":"1027823049943351297","text":"RT @MotherJones: Melania Trump\u2019s parents just became citizens through a process her husband wants to make illegal https:\/\/t.co\/CvbNs6pXvO h\u2026","source":"\u003ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003eTwitter for iPhone\u003c\/a\u003e","truncated":false,"in_reply_to_status_id":null,"in_reply_to_status_id_str":null,"in_reply_to_user_id":null,"in_reply_to_user_id_str":null,"in_reply_to_screen_name":null,"user":{"id":742502593943900163,"id_str":"742502593943900163","name":"Full Metal Finch","screen_name":"FullMetalFinch","location":"Planet Earth","url":null,"description":"Seeking life\u2019s beauty. Fighting its insanity. Maintaining my sense of humor. Blocked by Roseanne\u2744\ufe0f #Resist #FBR #MobilizeAgainstGunAtrocities","translator_type":"none","protected":false,"verified":false,"followers_count":8060,"friends_count":7986,"listed_count":20,"favourites_count":54427,"statuses_count":36563,"created_at":"Mon Jun 13 23:43:08 +0000 2016","utc_offset":null,"time_zone":null,"geo_enabled":false,"lang":"en","contributors_enabled":false,"is_translator":false,"profile_background_color":"F5F8FA","profile_background_image_url":"","profile_background_image_url_https":"","profile_background_tile":false,"profile_link_color":"1DA1F2","profile_sidebar_border_color":"C0DEED","profile_sidebar_fill_color":"DDEEF6","profile_text_color":"333333","profile_use_background_image":true,"profile_image_url":"http:\/\/pbs.twimg.com\/profile_images\/1010409635767914496\/0Kg7OeDu_normal.jpg","profile_image_url_https":"https:\/\/pbs.twimg.com\/profile_images\/1010409635767914496\/0Kg7OeDu_normal.jpg","profile_banner_url":"https:\/\/pbs.twimg.com\/profile_banners\/742502593943900163\/1529734010","default_profile":true,"default_profile_image":false,"following":null,"follow_request_sent":null,"notifications":null},"geo":null,"coordinates":null,"place":null,"contributors":null,"retweeted_status":{"created_at":"Thu Aug 09 23:47:12 +0000 2018","id":1027702869087203329,"id_str":"1027702869087203329","text":"Melania Trump\u2019s parents just became citizens through a process her husband wants to make illegal\u2026 https:\/\/t.co\/qxi8qGcXU4","display_text_range":[0,140],"source":"\u003ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003eSocialFlow\u003c\/a\u003e"}',

        new_list = []
        for index in test_list:
            new_list.insert(0, visualizer.determine_fields(index))

        for index in new_list:
            self.assertIn("_key", index)


    def test_fetch_data(self):
        visualizer = Main.Visualizer()
        visualizer.fetch_data('/Users/oguzgocmen/PycharmProjects/DataBoss/arangodb-visualizer/assets/raw-twitter-data.json')

    def test_insert_vertices(self):
        visualizer = Main.Visualizer()
        db = visualizer.open_database(8529)
        graph = visualizer.init_graph(db, "test-db")
        edge = visualizer.create_edges(graph, "test_edge", 'from', 'to')
        self.assertIsInstance(edge, arango.collection.EdgeCollection)

    def test_insert_edges(self):
        visualizer = Main.Visualizer()
        db = visualizer.open_database(8529)
        graph = visualizer.init_graph(db, "test-db")
        edge = visualizer.create_edges(graph, "test_edge", 'from', 'to')
        self.assertIsInstance(edge, arango.collection.EdgeCollection)

    def test_job_exec(self):
        self.fail()

    def test_insert_collection_helper(self):
        visualizer = Main.Visualizer()
        db = visualizer.open_database(8529)
        graph = visualizer.init_graph(db, "test-db")
        visualizer.insert_collection_helper(db, graph, '/Users/oguzgocmen/PycharmProjects/DataBoss/arangodb-visualizer/assets/raw-twitter-data.json')
        self.assertIsNotNone(graph)

    def test_remove_collection(self):
        visualizer = Main.Visualizer()
        db = visualizer.open_database(8529)
        graph = visualizer.init_graph(db, 'test-db')
        visualizer.remove_collection(db, graph.name)

    def test_insert_collection(self):
        visualizer = Main.Visualizer()
        db = visualizer.open_database(8529)
        graph = visualizer.init_graph(db, "test-db")
        visualizer.insert_collection(db, graph,
                                     '/Users/oguzgocmen/PycharmProjects/DataBoss/arangodb-visualizer/assets/raw-twitter-data.json')
        self.assertIsNotNone(graph)
