import json


class Filter():

    # method for filtering raw tweet data, can be adjusted later, determines manually for now
    def determine_fields(self, dataDict, *fields):

        filteredData = {}
        dataDict = json.loads(dataDict)

        # if key in dataDict.keys():
        # for field in fields:
        # user details
        if 'user' in dataDict.keys():
            filteredData['username'] = dataDict['user']['screen_name']

        # tweet content
        if 'id' in dataDict.keys():
            filteredData['_key'] = dataDict['id_str']
            filteredData['tweetid'] = dataDict['id_str']
        if 'text' in dataDict.keys():
            filteredData['tweet'] = dataDict['text']

        if 'entities' in dataDict.keys():
            # hashtag details
            filteredData['hashtags'] = []
            entities = dataDict['entities'].keys()
            if 'hashtags' in entities:
                for hashtag in dataDict['entities']['hashtags']:
                    filteredData['hashtags'].append(hashtag['text'])

            # mentioned details
            filteredData['mentionedpersons'] = []
            if 'user_mentions' in entities:
                for user in dataDict['entities']['user_mentions']:
                    currentUser = {}
                    currentUser['mentionedpersons'] = user['screen_name']
                    filteredData['mentionedpersons'].append(currentUser)

        return filteredData

    def fetch_data(self, path_to_file):
        # get twitter raw data from given file, filter after by calling determine_fields function
        # this determines all the attributes to create relations from in a tweet
        json_list = []
        with open(path_to_file) as json_file:
            lines = json_file.readlines()

            for line in lines:
                json_object = self.determine_fields(line)
                # try:
                if '_key' in json_object:
                    json_list.append(json_object)
                # except

        return json_list