from src import Filter
from arango import ArangoClient
from tweepy import StreamListener
import time


class Main(StreamListener):

    # method for opening connection, should be called first
    def open_database(self, db,  port):
        client = ArangoClient(protocol='http', host='localhost', port=port)
        sys_db = client.db(db, username='root', password='',verify=True)
        return sys_db

    # graph creation
    def create_graph(self, db, graph_name):
        if db.has_graph(graph_name):
            graph = db.graph(graph_name)
        else:
            graph = db.create_graph(graph_name)
        return graph

    # gets vertices if exists, creates if not
    def create_vertices(self, graph, collection_name):
        if graph.has_vertex_collection(collection_name) == True:
            vertices = graph.vertex_collection(collection_name)
        else:
            vertices = graph.create_vertex_collection(collection_name)
        return vertices

    # gets edges if exists, creates if not by taking from and to collections
    def create_edges(self, graph, collection_name, from_collection, to_collection):
        if graph.has_edge_definition(collection_name) == True:
            return graph.edge_collection(collection_name)
        else:
            return graph.create_edge_definition(edge_collection=collection_name,
                                                from_vertex_collections=[from_collection],
                                                to_vertex_collections=[to_collection])

    # graph init
    def init_graph(self, db, graph_name):
        graph = self.create_graph(db, graph_name)
        return graph

    # determine vertices to be inserted
    # put into a job list and pass to job_exec
    def insert_vertices(self, graph, json_list):
        vertex_job_list = {}

        # vertex collection insertion
        # iterate over determined fields
        #
        for field in json_list[0]:
            # some lines contain non-tweet objects, checking _key existence; tweetid table not needed
            if field != '_key' and field != 'tweetid':

                self.create_vertices(graph, field)
                print("Insertion to:", field)

                # empty init of temp variables to be used
                temp_dict = {}
                temp_dict[field] = []
                uniques = {}

                # get selected field in given object
                filtered_list = []
                for curr_item in json_list:
                    # mentionedpersons come as list of dicts, convert to list of screen_names
                    if field == 'mentionedpersons':
                        tmp = []
                        for item in curr_item[field]:
                            tmp.append(item[field])
                        curr_item[field] = tmp
                    # get desired field since every collection is created for just one field
                    filtered_dict = {k: v for (k, v) in curr_item.items() if field in k}
                    # exclude empty ones
                    if len(filtered_dict[field]) != 0:
                        filtered_list.append(filtered_dict)
                #############################################

                # hashtags and mentionedpersons come as list
                # divide lists with multiple elements and append them to the main list
                # erase main list after
                if field == 'hashtags' or field == 'mentionedpersons':
                    i = 0
                    while i < len(filtered_list):
                        if len(filtered_list[i][field]) > 1:
                            for item in filtered_list[i][field]:
                                filtered_list.append({field: [item]})
                            filtered_list.pop(i)
                            i -= 1
                        i += 1

                # ArangoDB does not check uniqueness of keys inside the table
                # Tables' data come as it is; hence multiple users, hashtags exist
                # This block ensures that unique elements exist in list to be inserted to db

                # If field to be inserted is a str or integer(not a list of smth.), evals this condition check
                if type(json_list[0][field]) != list:
                    uniques = [dict(tupleized) for tupleized in set(tuple(item.items()) for item in filtered_list)]
                # If field is a list of smth., eval this condition check
                else:

                    # iterate over list
                    for curr_item in filtered_list:
                        # unique check over list by looking to values,
                        # bulletproof for duplicate values e.g. same hashtag twice in one tweet
                        uniques = list(set(curr_item[field]))
                        # put whole hastags in single list
                        for item in uniques:
                            temp_dict[field].append(item)
                        uniques = []

                    # bulletproof for duplicate values e.g. same hashtag multiple times in multiple tweets
                    filtered_list = list(set(temp_dict[field]))

                    # append to a new list for insertion
                    for item in filtered_list:
                        uniques.append({field: item})

                # uniques must be list of dictionaries to make this work

                # loop for assigning keys to use in creating relations and searching inside db
                for obj in uniques:
                    if 'tweetid' in obj and 'tweet' not in obj:
                        pass
                    # only tweet collection contain tweetid, assign tweetid as key
                    elif 'tweetid' in obj:
                        obj['_key'] = obj['tweetid']
                    # in other collections every row is unique, so key can be their value
                    else:
                        obj['_key'] = obj[field]
                # add to job list with collection name along with their values
                vertex_job_list[field] = uniques

        return vertex_job_list

    # determine edges to be inserted
    # put into a job list and pass to job_exec
    def insert_edges(self, graph, json_list):
        edge_job_list = {}
        keys = []

        # edge collection insertion
        # iterate over determined fields to create relations
        # In ArangoDB edge collections(relations) has to contain '_from' and '_to' fields to specify relation
        # these two fields structure is --> collection_name/_key
        for item in json_list[0]:
            # get keys
            if item == 'hashtags' or item == 'mentionedpersons':
                key_table = graph.vertex_collection(item)
                for key in key_table:
                    keys.append(key['_key'])
            # tweet collection excluded since every collection will go through that collection
            # tweet collection is also the lookup collection to construct relations between collections
            if item != '_key' and item != 'tweetid' and item != 'tweet':
                if type(json_list[0][item]) != list:
                    edges_name = ''
                    edges = []
                    if item == 'username':
                        # create edge collection
                        edges_name = 'username-tweet'
                        self.create_edges(graph, edges_name, 'username', 'tweet')

                        # iterate over base list to create relation objects
                        for curr_item in json_list:
                            objj = {}
                            objj['_from'] = 'username/' + curr_item['username']  # key in username collection
                            objj['_to'] = 'tweet/' + curr_item['_key']  # key in tweet collection
                            edges.append(objj)

                    elif item != 'tweet':
                        # create edge collection
                        edges_name = 'tweet-' + item
                        self.create_edges(graph, edges_name, 'tweet', item)

                        # iterate over base list to create relation objects
                        for curr_item in json_list:
                            objj = {}
                            objj['_from'] = 'tweet/' + curr_item['_key']  # key in tweet collection
                            objj['_to'] = item + '/' + curr_item['_key']  # key in given item collection
                            edges.append(objj)

                else:
                    # create edge collection
                    edges_name = 'tweet-' + item
                    self.create_edges(graph, edges_name, 'tweet', item)
                    edges = []

                    # iterate over base list to create relation objects
                    for curr_item in json_list:
                        for element in curr_item[item]:
                            # check if hashtags is actually in db,
                            # ArangoDB _key field accepts ASCII characters where some contain UTF-8
                            if element in keys:
                                objj = {}
                                objj['_from'] = 'tweet/' + curr_item['_key']  # key in tweet collection
                                objj['_to'] = item + '/' + element  # key in given item collection
                                edges.append(objj)

                # add to job list with collection name along with their values
                edge_job_list[edges_name] = edges

        return edge_job_list

    # executor for given job lists in async db
    def job_exec(self, db, job_list):

        async_db = db.begin_async_execution(return_result=True)
        for key, value in job_list.items():
            async_col = async_db.collection(key)
            async_col.insert(value)

    def query_exec(self, db, query):
        # Get the AQL API wrapper.
        aql = db.aql

        # Retrieve the execution plan without running the query.
        aql.explain(query)

        # Validate the query without executing it.
        aql.validate(query)

        # Execute the query
        cursor = db.aql.execute(query)
        # Iterate through the result cursor
        elements = [doc for doc in cursor]

        for i in range(len(elements)):
            print("Node ", (i+1), ":")
            print(elements[i])

    # helper function for collection insert
    def insert_collection_helper(self, db, graph, path_to_file, *fields):
        start = time.time()

        filter = Filter.Filter()
        json_list = filter.fetch_data(path_to_file)

        end = time.time()
        print("Fetching data from file took: " + str(end - start))

        start = time.time()
        vertex_job_list = self.insert_vertices(graph, json_list)
        end = time.time()
        print("Init of vertex collections took: " + str(end - start))

        start = time.time()
        # async execution of jobs in job list
        self.job_exec(db,vertex_job_list)
        end = time.time()
        print("Insertion of vertex collections took: " + str(end - start))

        # relations between users and tweets
        start = time.time()
        edge_job_list = self.insert_edges(graph, json_list)
        end = time.time()
        print("Init of edge collections took: " + str(end - start))

        start = time.time()
        # async execution of jobs in job list, sometime get corrupt don't know why
        self.job_exec(db, edge_job_list)
        end = time.time()
        print("Insertion of edge collections took: " + str(end - start))


    # function for collection removal, all collections removed after call
    def remove_collection(self, db, graph):

        db.delete_graph(graph)

    # function for collection insertion
    def insert_collection(self, db, graph, path_to_file, *fields):

        # while True:
        print("insertion has begun")
        self.insert_collection_helper(db, graph, path_to_file, *fields)
        print("insertion has finished")
        #db.shutdown()
        # time.sleep(15)
        # print("deletion has begun")
        #self.remove_collection(db, graph.name)
        # print("deletion has finished")
        # time.sleep(30)