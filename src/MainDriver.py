from src import Main

# namings
graph_name='test-graph'
user_collection = 'users'
hashtag_collection = 'hashtags'
edge_collection = 'test-edges'

# driver
visualizer = Main.Main()
db = 'test-db'
db = visualizer.open_database(db, 8529)
graph = visualizer.init_graph(db, graph_name)

visualizer.insert_collection(db, graph,
                             '/Users/oguzgocmen/PycharmProjects/DataBoss/arangodb-visualizer/assets/raw-twitter-data-2.json')

visualizer.query_exec(db, "FOR vertex,edge IN ANY SHORTEST_PATH "
                          "'mentionedpersons/POTUS' TO 'hashtags/cnn' GRAPH 'test-graph' RETURN vertex")